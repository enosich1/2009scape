package api

interface PersistWorld {
    fun saveWorld()
    fun parseWorld()
}